<x-layout>
    <section class="px-6 py-8">
        <main class="max-w-lg mx-auto bg-gray-100 border border-gray-200 p-6 rounded-xl">
            <form method="POST" action="{{ route('register.store') }}">
                @csrf
                <h1 class="text-xl font-bold text-center">Register!</h1>
                <x-form.input name="username" type="text" required="true" autocomplete="username"/>
                <x-form.input name="phone_number" type="tel" required="true" autocomplete="tel"/>
                <x-form.button>
                    Register
                </x-form.button>
            </form>
        </main>
    </section>
</x-layout>
