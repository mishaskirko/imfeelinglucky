<x-layout>
    <section class="px-6 py-8 bg-gray-100 text-center">
        <p class="text-lg font-medium mb-4">Welcome, your link for the game is:</p>
        <a href="{{ url(route('game.show', auth()->user()->game()->first()->identifier)) }}"
           class="text-blue-600 hover:underline">
            {{ url(route('game.show', auth()->user()->game()->first()->identifier)) }}
        </a>
    </section>
</x-layout>
