<x-layout>
    <section class="px-6 py-8 text-center">
        <form method="POST" action="{{ route('imfeelinglucky') }}">
            @csrf
            <x-form.button type="submit"
                           class="bg-blue-500 text-white px-4 py-2 rounded hover:bg-blue-600 focus:outline-none focus:shadow-outline-blue">
                imfeelinglucky
            </x-form.button>
        </form>
        @php
            $randomNumber = session('randomNumber', 0);
            $amount = session('amount', 0);
            $result = session('result', '');
        @endphp
        @if (!empty($result))
            <div class="text-center mt-4">
                <h2>Your number is {{ $randomNumber }}</h2>
                <h2>{{ $result }}</h2>
                <p>You {{ ($amount > 0) ?
                    'won ' . $amount :
                    'did not win anything.'
                    }}
                </p>
            </div>
        @endif
        <div class="flex justify-center mb-4 py-2 px-4">
            <form method="POST" action="{{ route('game.link.update') }}">
                @csrf
                <x-form.button type="submit">
                    Update Link
                </x-form.button>
            </form>
            <form method="POST" action="{{ route('game.link.destroy') }}">
                @csrf
                <x-form.button type="submit" class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">
                    Delete Link
                </x-form.button>
            </form>
        </div>
        <div x-data="{ showHistory: false }" @click.away="showHistory = false" class="relative">
            <x-form.button @click="showHistory = ! showHistory">History</x-form.button>
            <div x-show="showHistory"
                 class="py-2 absolute bg-gray-100 w-full mt-2 rounded-xl z-50 overflow-auto max-h-52"
                 style="display: none">
                @foreach(session('gameHistory', []) as $game)
                    <li>Number: {{$game['randomNumber']}}, Result: {{$game['result'] }},
                        Amount: {{ $game['amount'] }}</li>
                @endforeach
            </div>
        </div>
    </section>
</x-layout>
