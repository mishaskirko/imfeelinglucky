<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;

class Game extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'link',
        'expiration_date'
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param int $user_id
     * @return bool
     */
    public function create(int $user_id): bool
    {
        $this->user_id = $user_id;
        $this->identifier = Str::random();
        $this->expiration_date = Carbon::now()->addDays('7');
        return $this->save();
    }

    /**
     * Check if the game is expired
     *
     * @param string $identifier
     * @return bool
     */
    public function isExpired(string $identifier): bool
    {
        $game = Game::query()
            ->where('identifier', '=', $identifier)
            ->select('expiration_date')
            ->firstOrFail();

        return now() > $game->expiration_date;
    }


}
