<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\StoreUserRequest;
use App\Models\Game;
use App\Models\User;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /**
     * @return Factory|View|Application
     */
    public function show(): Factory|View|Application
    {
        return view('auth.register.show');
    }

    /**
     * @param StoreUserRequest $request
     * @param User $user
     * @param Game $game
     * @return string|Redirector|RedirectResponse|Application
     */
    public function store(
        StoreUserRequest $request,
        User             $user,
        Game             $game
    ): string|Redirector|RedirectResponse|Application
    {
        try {
            $newUser = $user->newModelQuery()->create($request->validated());
            $game->create($newUser->id);
            Auth::login($newUser);
            return redirect(route('game.link.show'))->with('user');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
