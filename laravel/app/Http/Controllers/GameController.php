<?php

namespace App\Http\Controllers;

use App\Models\Game;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Str;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreGameRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Game $game, string $identifier)
    {
        if ($game->isExpired($identifier)) {
            return view('game.expired.show');
        }
        return view('game.show');
    }

    /**
     * Display the specified resource.
     * @return Factory|View|Application
     */
    public function showGameLink(): Factory|View|Application
    {
        return view('game.link.show');
    }

    public function play(): RedirectResponse
    {
        $randomNumber = rand(1, 1000);
        $result = ($randomNumber % 2 === 0) ? 'Win' : 'Lose';
        $amount = 0;

        if ($result === 'Win') {
            $amount = match (true) {
                $randomNumber > 900 => round($randomNumber * 0.7),
                $randomNumber > 600 => round($randomNumber * 0.5),
                $randomNumber > 300 => round($randomNumber * 0.3),
                default => round($randomNumber * 0.1)
            };
        }

        $gameHistory = session('gameHistory', []);

        array_unshift($gameHistory, ['randomNumber' => $randomNumber, 'amount' => $amount, 'result' => $result]);

        if (count($gameHistory) > 3) {
            array_pop($gameHistory);
        }

        session(['gameHistory' => $gameHistory]);

        return redirect()->back()->with(['randomNumber' => $randomNumber, 'amount' => $amount, 'result' => $result]);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Game $game)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateGameRequest $request, Game $game)
    {
//
    }

    /**
     * Update the specified resource in storage.
     */
    public function updateGameLink()
    {
        $game = auth()->user()->game()->first();
        $game->expiration_date = Carbon::now()->addDays(7);
        $game->identifier = Str::random();
        $game->save();

        return redirect(route('game.link.show'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Game $game)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function destroyGameLink()
    {
        $game = auth()->user()->game()->first();
        $game->delete();

        return redirect(route('register.show'));
    }
}
