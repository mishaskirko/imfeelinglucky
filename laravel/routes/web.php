<?php

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\GameController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::controller(RegisterController::class)->group(function () {
    Route::get('/', 'show')->name('register.show');
    Route::post('/', 'store')->name('register.store');
});

Route::controller(GameController::class)->group(function () {
    Route::get('/game/link', 'showGameLink')->name('game.link.show');
    Route::post('/game/link/update', 'updateGameLink')->name('game.link.update');
    Route::post('/game/link/destroy', 'destroyGameLink')->name('game.link.destroy');
    Route::get('/game/{identifier}', 'show')->name('game.show');
    Route::post('/imfeelinglucky', 'play')->name('imfeelinglucky');
})->middleware('auth');
